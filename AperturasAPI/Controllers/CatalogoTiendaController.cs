﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoTiendaController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoTienda
        [ResponseType(typeof(CAT_Tienda))]
        public IHttpActionResult GetCat_Tienda()
        {
            var query = (from p in db.Set<CAT_Tienda>()
                         select new
                         {
                             p.tienda_id,
                             p.nombre_tienda
                         }).ToList()
                       .Select(x => new CAT_Tienda
                       {
                           nombre_tienda = x.nombre_tienda.Trim(),
                           tienda_id = x.tienda_id
                       });
            return Ok(query);
        }

        // GET: api/CatalogoTienda/5
        [ResponseType(typeof(CAT_Tienda))]
        public IHttpActionResult GetCAT_Tienda(int id)
        {
            var query = (from p in db.Set<CAT_Tienda>()
                         select new
                         {
                             p.tienda_id,
                             p.nombre_tienda
                         }).ToList()
                       .Select(x => new CAT_Tienda
                       {
                           nombre_tienda = x.nombre_tienda.Trim(),
                           tienda_id = x.tienda_id
                       }).Where(x => x.tienda_id == id);

            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoTienda/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_Tienda(int id, CAT_Tienda cAT_Tienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_Tienda.tienda_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_Tienda).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_TiendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoTienda
        [ResponseType(typeof(CAT_Tienda))]
        public IHttpActionResult PostCAT_Tienda(CAT_Tienda cAT_Tienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_Tienda.Add(cAT_Tienda);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_Tienda.tienda_id }, cAT_Tienda);
        }

        // DELETE: api/CatalogoTienda/5
        [ResponseType(typeof(CAT_Tienda))]
        public IHttpActionResult DeleteCAT_Tienda(int id)
        {
            CAT_Tienda cAT_Tienda = db.CAT_Tienda.Find(id);
            if (cAT_Tienda == null)
            {
                return NotFound();
            }

            db.CAT_Tienda.Remove(cAT_Tienda);
            db.SaveChanges();

            return Ok(cAT_Tienda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_TiendaExists(int id)
        {
            return db.CAT_Tienda.Count(e => e.tienda_id == id) > 0;
        }
    }
}