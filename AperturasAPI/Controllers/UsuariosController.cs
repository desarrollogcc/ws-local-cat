﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class UsuariosController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/Usuarios
        [Route("api/Login")]
        [HttpPost]
        [ResponseType(typeof(Response))]
        public IHttpActionResult Login(Login login)
        {
            var log = db.Usuario.Where(x => x.empleado_usuario.Equals(login.User) && x.password.Equals(login.Password)).FirstOrDefault();
            Response RES = new Response();
            if (log == null)
            {
                return NotFound();
            }
            else
            {
                RES.Status = "Success";
                RES.Message = "Login Successfully";
                return Ok(RES);
            }
        }

        // GET: api/Usuarios/5
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult GetUsuario(int id)
        {
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(usuario);
        }

		[ResponseType(typeof(Usuario))]
		public IHttpActionResult GetUsuario()
		{
			var query = (from u in db.Set<Usuario>()
						 select new
						 {
							 u.no_empleado,
							 u.nombre,
							 u.email,
							 u.perfil,
							 u.cadena,
							 u.area_asignada,
							 u.activo,
							 u.reportes,
							 u.empleado_usuario
						 }).ToList()
						.Select(x => new Usuario
						{
							no_empleado = x.no_empleado,
							nombre = x.nombre.Trim(),
							email = x.email.Trim(),
							perfil = x.perfil.Trim(),
							cadena = x.cadena,
							area_asignada = x.area_asignada,
							activo = x.activo,
							reportes = x.reportes,
							empleado_usuario = x.empleado_usuario.Trim()
						});

			return Ok(query);
		}

		// PUT: api/Usuarios/5
		[ResponseType(typeof(void))]
        public IHttpActionResult PutUsuario(int id, Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usuario.no_empleado)
            {
                return BadRequest();
            }

            db.Entry(usuario).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Usuarios
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult PostUsuario(Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Usuario.Add(usuario);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UsuarioExists(usuario.no_empleado))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = usuario.no_empleado }, usuario);
        }

        // DELETE: api/Usuarios/5
        [ResponseType(typeof(Usuario))]
        public IHttpActionResult DeleteUsuario(int id)
        {
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return NotFound();
            }

            db.Usuario.Remove(usuario);
            db.SaveChanges();

            return Ok(usuario);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsuarioExists(int id)
        {
            return db.Usuario.Count(e => e.no_empleado == id) > 0;
        }
    }
}