﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	
	public class CatalogoGerenteZonaController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoGerenteZona
        [ResponseType(typeof(CAT_GerenteZona))]
        public IHttpActionResult GetCAT_GerenteZona()
        {
            var query = (from p in db.Set<CAT_GerenteZona>()
                         select new
                         {
                             p.nombre_gerente_zona,
                             p.gerente_zona_id
                         }).ToList()
                        .Select(x => new CAT_GerenteZona
                        {
                            nombre_gerente_zona = x.nombre_gerente_zona.Trim(),
                            gerente_zona_id = x.gerente_zona_id
                        });

            return Ok(query);
        }

        // GET: api/CatalogoGerenteZona/5
        [ResponseType(typeof(CAT_GerenteZona))]
        public IHttpActionResult GetCAT_GerenteZona(int id)
        {

            var query = (from p in db.Set<CAT_GerenteZona>()
                         select new
                         {
                             p.nombre_gerente_zona,
                             p.gerente_zona_id
                         }).ToList()
                        .Select(x => new CAT_GerenteZona
                        {
                            nombre_gerente_zona = x.nombre_gerente_zona.Trim(),
                            gerente_zona_id = x.gerente_zona_id
                        }).Where(x => x.gerente_zona_id == id).FirstOrDefault();
            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoGerenteZona/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_GerenteZona(int id, CAT_GerenteZona cAT_GerenteZona)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_GerenteZona.gerente_zona_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_GerenteZona).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_GerenteZonaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoGerenteZona
        [ResponseType(typeof(CAT_GerenteZona))]
        public IHttpActionResult PostCAT_GerenteZona(CAT_GerenteZona cAT_GerenteZona)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_GerenteZona.Add(cAT_GerenteZona);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_GerenteZona.gerente_zona_id }, cAT_GerenteZona);
        }

        // DELETE: api/CatalogoGerenteZona/5
        [ResponseType(typeof(CAT_GerenteZona))]
        public IHttpActionResult DeleteCAT_GerenteZona(int id)
        {
            CAT_GerenteZona cAT_GerenteZona = db.CAT_GerenteZona.Find(id);
            if (cAT_GerenteZona == null)
            {
                return NotFound();
            }

            db.CAT_GerenteZona.Remove(cAT_GerenteZona);
            db.SaveChanges();

            return Ok(cAT_GerenteZona);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_GerenteZonaExists(int id)
        {
            return db.CAT_GerenteZona.Count(e => e.gerente_zona_id == id) > 0;
        }
    }
}