﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class ProyectosController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/Proyectos
        [ResponseType(typeof(Response))]
        public IHttpActionResult GetProyecto()
        {
            var query = from Proyecto in db.Proyecto
                        join CAT_Tienda in db.CAT_Tienda on Proyecto.tienda equals CAT_Tienda.tienda_id
                        join CAT_Proyecto in db.CAT_Proyecto on Proyecto.tipo_proyecto equals CAT_Proyecto.proyecto_id
                        join CAT_TipoTienda in db.CAT_TipoTienda on Proyecto.tipo_tienda equals CAT_TipoTienda.tipo_tienda_id
                        join CAT_TipoInmueble in db.CAT_TipoInmueble on Proyecto.tipo_inmueble equals CAT_TipoInmueble.tipo_inmueble_id
                        select new ProyectoInfo
                        {
                            ID = Proyecto.proyecto_id,
                            Tienda = CAT_Tienda.nombre_tienda.Trim(),
                            TipoPlan = CAT_Proyecto.tipo_proyecto.Trim(),
                            TipoTienda = CAT_TipoTienda.nombre_tipo_tienda.Trim(),
                            TipoInmueble = CAT_TipoInmueble.nombre_tipo_inmueble.Trim(),
                            TiendaEspejo = Proyecto.tienda_espejo,
                            FechaApertura = Proyecto.fecha_apertura,
                            Direccion = Proyecto.direccion_tienda.Trim(),
                            Estatus = Proyecto.estado,
                            EnComunicado = Proyecto.en_comunicado_gral
                        };
            return Ok(query);
        }

        // GET: api/Proyectos/5
        [ResponseType(typeof(ProyectoInfoFull))]
        public IHttpActionResult GetProyecto(int id)
        {
            var query = (from Proyecto in db.Proyecto
                        join CAT_Tienda in db.CAT_Tienda on Proyecto.tienda equals CAT_Tienda.tienda_id
                        join CAT_Proyecto in db.CAT_Proyecto on Proyecto.tipo_proyecto equals CAT_Proyecto.proyecto_id
                        join CAT_TipoTienda in db.CAT_TipoTienda on Proyecto.tipo_tienda equals CAT_TipoTienda.tipo_tienda_id
                        join CAT_TipoInmueble in db.CAT_TipoInmueble on Proyecto.tipo_inmueble equals CAT_TipoInmueble.tipo_inmueble_id
                        join CAT_FormatoTienda in db.CAT_FormatoTienda on Proyecto.formato_tienda equals CAT_FormatoTienda.formato_tienda_id
                        join CAT_Subdireccion in db.CAT_Subdireccion on Proyecto.subdireccion equals CAT_Subdireccion.subdireccion_id
                        join CAT_ZonaTienda in db.CAT_ZonaTienda on Proyecto.zona_tienda equals CAT_ZonaTienda.zona_tienda_id
                        join CAT_Temporada in db.CAT_Temporada on Proyecto.temporada equals CAT_Temporada.temporada_id
                        join CAT_GerenteZona in db.CAT_GerenteZona on Proyecto.gerente_zona equals CAT_GerenteZona.gerente_zona_id
                        where Proyecto.proyecto_id == id
                        select new ProyectoInfoFull
                        {
                            ID = Proyecto.proyecto_id,
                            TipoProyecto = CAT_Proyecto.proyecto_id,
                            FechaApertura = Proyecto.fecha_apertura,
                            FechaInicioObra = Proyecto.fecha_inicio_obra,
                            Tienda = CAT_Tienda.tienda_id,
                            NombreTienda = Proyecto.nombre_tienda.Trim(),
                            Formato = CAT_FormatoTienda.formato_tienda_id,
                            Subdireccion = CAT_Subdireccion.subdireccion_id,
                            GerenteZona = CAT_GerenteZona.gerente_zona_id,
                            TiendaEspejo = Proyecto.tienda_espejo,
                            TipoTienda = CAT_TipoTienda.tipo_tienda_id,
                            TipoInmueble = CAT_TipoInmueble.tipo_inmueble_id,
                            ZonaTienda = CAT_ZonaTienda.zona_tienda_id,
                            Direccion = Proyecto.direccion_tienda.Trim(),
                            ObservacionesEspejo = Proyecto.observaciones_espejo.Trim(),
                            ObservacionesGral = Proyecto.observaciones_generales.Trim(),
                        }).FirstOrDefault();
            if(query == null)
            {
                return NotFound();
            }
            return Ok(query);
        }

    //    .FirstOrDefault();
    //        if(query == null)
    //        {
    //            return NotFound();
    //}
    //        return Ok(query);


    // PUT: api/Proyectos/5
    [ResponseType(typeof(void))]
        public IHttpActionResult PutProyecto(int id, Proyecto proyecto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proyecto.proyecto_id)
            {
                return BadRequest();
            }

            db.Entry(proyecto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProyectoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Proyectos
        [ResponseType(typeof(Proyecto))]
        public IHttpActionResult PostProyecto(Proyecto proyecto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Proyecto.Add(proyecto);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = proyecto.proyecto_id }, proyecto);
        }

        // DELETE: api/Proyectos/5
        [ResponseType(typeof(Proyecto))]
        public IHttpActionResult DeleteProyecto(int id)
        {
            Proyecto proyecto = db.Proyecto.Find(id);
            if (proyecto == null)
            {
                return NotFound();
            }

            db.Proyecto.Remove(proyecto);
            db.SaveChanges();

            return Ok(proyecto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProyectoExists(int id)
        {
            return db.Proyecto.Count(e => e.proyecto_id == id) > 0;
        }
    }
}