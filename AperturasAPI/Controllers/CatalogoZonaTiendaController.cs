﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoZonaTiendaController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoZonaTienda
        [ResponseType(typeof(CAT_ZonaTienda))]
        public IHttpActionResult GetCAT_ZonaTienda()
        {
            var query = (from p in db.Set<CAT_ZonaTienda>()
                         select new
                         {
                             p.nombre_zona_tienda,
                             p.zona_tienda_id
                         }).ToList()
                        .Select(x => new CAT_ZonaTienda
                        {
                            nombre_zona_tienda = x.nombre_zona_tienda.Trim(),
                            zona_tienda_id = x.zona_tienda_id
                        });

            return Ok(query);
        }

        // GET: api/CatalogoZonaTienda/5
        [ResponseType(typeof(CAT_ZonaTienda))]
        public IHttpActionResult GetCAT_ZonaTienda(int id)
        {
            var query = (from p in db.Set<CAT_ZonaTienda>()
                         select new
                         {
                             p.nombre_zona_tienda,
                             p.zona_tienda_id
                         }).ToList()
                        .Select(x => new CAT_ZonaTienda
                        {
                            nombre_zona_tienda = x.nombre_zona_tienda.Trim(),
                            zona_tienda_id = x.zona_tienda_id
                        }).Where(x => x.zona_tienda_id == id).FirstOrDefault();
            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoZonaTienda/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_ZonaTienda(int id, CAT_ZonaTienda cAT_ZonaTienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_ZonaTienda.zona_tienda_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_ZonaTienda).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_ZonaTiendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoZonaTienda
        [ResponseType(typeof(CAT_ZonaTienda))]
        public IHttpActionResult PostCAT_ZonaTienda(CAT_ZonaTienda cAT_ZonaTienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_ZonaTienda.Add(cAT_ZonaTienda);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_ZonaTienda.zona_tienda_id }, cAT_ZonaTienda);
        }

        // DELETE: api/CatalogoZonaTienda/5
        [ResponseType(typeof(CAT_ZonaTienda))]
        public IHttpActionResult DeleteCAT_ZonaTienda(int id)
        {
            CAT_ZonaTienda cAT_ZonaTienda = db.CAT_ZonaTienda.Find(id);
            if (cAT_ZonaTienda == null)
            {
                return NotFound();
            }

            db.CAT_ZonaTienda.Remove(cAT_ZonaTienda);
            db.SaveChanges();

            return Ok(cAT_ZonaTienda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_ZonaTiendaExists(int id)
        {
            return db.CAT_ZonaTienda.Count(e => e.zona_tienda_id == id) > 0;
        }
    }
}