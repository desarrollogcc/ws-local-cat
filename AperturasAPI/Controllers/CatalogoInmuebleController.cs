﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoInmuebleController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoInmueble
        [ResponseType(typeof(CAT_TipoInmueble))]
        public IHttpActionResult GetCAT_TipoInmueble()
        {
            var query = (from p in db.Set<CAT_TipoInmueble>()
                         select new
                         {
                             p.tipo_inmueble_id,
                             p.nombre_tipo_inmueble
                         }).ToList()
                       .Select(x => new CAT_TipoInmueble
                       {
                           nombre_tipo_inmueble = x.nombre_tipo_inmueble.Trim(),
                           tipo_inmueble_id = x.tipo_inmueble_id
                       });
            return Ok(query);
        }

        // GET: api/CatalogoInmueble/5
        [ResponseType(typeof(CAT_TipoInmueble))]
        public IHttpActionResult GetCAT_TipoInmueble(int id)
        {
            var query = (from p in db.Set<CAT_TipoInmueble>()
                         select new
                         {
                             p.tipo_inmueble_id,
                             p.nombre_tipo_inmueble
                         }).ToList()
                       .Select(x => new CAT_TipoInmueble
                       {
                           nombre_tipo_inmueble = x.nombre_tipo_inmueble.Trim(),
                           tipo_inmueble_id = x.tipo_inmueble_id
                       }).Where(x => x.tipo_inmueble_id == id);

            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoInmueble/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_TipoInmueble(int id, CAT_TipoInmueble cAT_TipoInmueble)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_TipoInmueble.tipo_inmueble_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_TipoInmueble).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_TipoInmuebleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoInmueble
        [ResponseType(typeof(CAT_TipoInmueble))]
        public IHttpActionResult PostCAT_TipoInmueble(CAT_TipoInmueble cAT_TipoInmueble)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_TipoInmueble.Add(cAT_TipoInmueble);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_TipoInmueble.tipo_inmueble_id }, cAT_TipoInmueble);
        }

        // DELETE: api/CatalogoInmueble/5
        [ResponseType(typeof(CAT_TipoInmueble))]
        public IHttpActionResult DeleteCAT_TipoInmueble(int id)
        {
            CAT_TipoInmueble cAT_TipoInmueble = db.CAT_TipoInmueble.Find(id);
            if (cAT_TipoInmueble == null)
            {
                return NotFound();
            }

            db.CAT_TipoInmueble.Remove(cAT_TipoInmueble);
            db.SaveChanges();

            return Ok(cAT_TipoInmueble);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_TipoInmuebleExists(int id)
        {
            return db.CAT_TipoInmueble.Count(e => e.tipo_inmueble_id == id) > 0;
        }
    }
}