﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoFormatoTiendaController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoFormatoTienda
        [ResponseType(typeof(CAT_FormatoTienda))]
        public IHttpActionResult GetCAT_FormatoTienda()
        {
            var query = (from p in db.Set<CAT_FormatoTienda>()
                         select new
                         {
                             p.nombre_formato_tienda,
                             p.formato_tienda_id
                         }).ToList()
                        .Select(x => new CAT_FormatoTienda
                        {
                            nombre_formato_tienda = x.nombre_formato_tienda.Trim(),
                            formato_tienda_id = x.formato_tienda_id
                        });

            return Ok(query);
        }

        // GET: api/CatalogoFormatoTienda/5
        [ResponseType(typeof(CAT_FormatoTienda))]
        public IHttpActionResult GetCAT_FormatoTienda(int id)
        {
            var query = (from p in db.Set<CAT_FormatoTienda>()
                         select new
                         {
                             p.nombre_formato_tienda,
                             p.formato_tienda_id
                         }).ToList()
                        .Select(x => new CAT_FormatoTienda
                        {
                            nombre_formato_tienda = x.nombre_formato_tienda.Trim(),
                            formato_tienda_id = x.formato_tienda_id
                        }).Where(x => x.formato_tienda_id == id).FirstOrDefault();
            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoFormatoTienda/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_FormatoTienda(int id, CAT_FormatoTienda cAT_FormatoTienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_FormatoTienda.formato_tienda_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_FormatoTienda).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_FormatoTiendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoFormatoTienda
        [ResponseType(typeof(CAT_FormatoTienda))]
        public IHttpActionResult PostCAT_FormatoTienda(CAT_FormatoTienda cAT_FormatoTienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_FormatoTienda.Add(cAT_FormatoTienda);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_FormatoTienda.formato_tienda_id }, cAT_FormatoTienda);
        }

        // DELETE: api/CatalogoFormatoTienda/5
        [ResponseType(typeof(CAT_FormatoTienda))]
        public IHttpActionResult DeleteCAT_FormatoTienda(int id)
        {
            CAT_FormatoTienda cAT_FormatoTienda = db.CAT_FormatoTienda.Find(id);
            if (cAT_FormatoTienda == null)
            {
                return NotFound();
            }

            db.CAT_FormatoTienda.Remove(cAT_FormatoTienda);
            db.SaveChanges();

            return Ok(cAT_FormatoTienda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_FormatoTiendaExists(int id)
        {
            return db.CAT_FormatoTienda.Count(e => e.formato_tienda_id == id) > 0;
        }
    }
}