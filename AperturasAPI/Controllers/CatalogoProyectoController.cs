﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{

	public class CatalogoProyectoController : ApiController
	{
		private AperturasEntities db = new AperturasEntities();

		// GET: api/CatalogoProyecto

		[ResponseType(typeof(CAT_Proyecto))]
		public IHttpActionResult GetCAT_Proyecto()
		{
			var query = (from p in db.Set<CAT_Proyecto>()
						 select new
						 {
							 p.proyecto_id,
							 p.tipo_proyecto
						 }).ToList()
						.Select(x => new CAT_Proyecto
						{
							tipo_proyecto = x.tipo_proyecto.Trim(),
							proyecto_id = x.proyecto_id
						});

			return Ok(query);
		}

		// DELETE: api/CatalogoProyecto/5
		[ResponseType(typeof(CAT_Proyecto))]
		public IHttpActionResult DeleteCAT_Proyecto(int id)
		{
			CAT_Proyecto cAT_Proyecto = db.CAT_Proyecto.Find(id);

			if (cAT_Proyecto == null)
			{
				return NotFound();
			}

			db.CAT_Proyecto.Remove(cAT_Proyecto);
			db.SaveChanges();

			return Ok(cAT_Proyecto);
		}

		// GET: api/CatalogoProyecto/5
		[ResponseType(typeof(CAT_Proyecto))]
		public IHttpActionResult GetCAT_Proyecto(int id)
		{

			var query = (from p in db.Set<CAT_Proyecto>()
						 select new
						 {
							 p.tipo_proyecto,
							 p.proyecto_id
						 }).ToList()
						.Select(x => new CAT_Proyecto
						{
							tipo_proyecto = x.tipo_proyecto.Trim(),
							proyecto_id = x.proyecto_id
						}).Where(x => x.proyecto_id == id).FirstOrDefault();
			if (query == null)
			{
				return NotFound();
			}

			return Ok(query);
		}

		// PUT: api/CatalogoProyecto/5
		[ResponseType(typeof(void))]
		public IHttpActionResult PutCAT_Proyecto(int id, CAT_Proyecto cAT_Proyecto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != cAT_Proyecto.proyecto_id)
			{
				return BadRequest();
			}

			db.Entry(cAT_Proyecto).State = EntityState.Modified;

			try
			{
				db.SaveChanges();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!CAT_ProyectoExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/CatalogoProyecto
		[ResponseType(typeof(CAT_Proyecto))]
		public IHttpActionResult PostCAT_Proyecto(CAT_Proyecto cAT_Proyecto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			db.CAT_Proyecto.Add(cAT_Proyecto);
			db.SaveChanges();

			return CreatedAtRoute("DefaultApi", new { id = cAT_Proyecto.proyecto_id }, cAT_Proyecto);
		}

		
		

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool CAT_ProyectoExists(int id)
		{
			return db.CAT_Proyecto.Count(e => e.proyecto_id == id) > 0;
		}
	}
}