﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class TareaProyectosController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        //// GET: api/TareaProyectoes
        //[ResponseType(typeof(TareaProyecto))]
        //public IHttpActionResult GetTareaProyecto()
        //{
        //    var query = (from p in db.Set<TareaProyecto>()
        //                 select new
        //                 {
        //                     p.tarea_id,
        //                     p.proyecto_id,
        //                     p.tarea_by_proy_id,
        //                     p.precedente,
        //                     p.fecha_compromiso,
        //                     p.fecha_real_cierre,
        //                     p.concluido,
        //                     p.actividad,
        //                     p.area_responsable,
        //                     p.dato_cierre
        //                 }).ToList()
        //                .Select(x => new TareaProyecto
        //                {
        //                    tarea_id = x.tarea_id,
        //                    tarea_by_proy_id = x.tarea_by_proy_id,
        //                    proyecto_id = x.proyecto_id,
        //                    precedente = x.precedente,
        //                    fecha_compromiso = x.fecha_compromiso,
        //                    fecha_real_cierre = x.fecha_real_cierre,
        //                    concluido = x.concluido,
        //                    actividad = x.actividad,
        //                    area_responsable = x.area_responsable,
        //                    dato_cierre = x.dato_cierre
        //                });

        //    return Ok(query);
        //}

        // GET: api/TareaProyectoes/5
        [ResponseType(typeof(TareaProyecto))]
        public IHttpActionResult GetTareaProyecto(int id)
        {
            var query = (from p in db.Set<TareaProyecto>()
                         select new
                         {
                             p.tarea_id,
                             p.proyecto_id,
                             p.tarea_by_proy_id,
                             p.precedente,
                             p.fecha_compromiso,
                             p.fecha_real_cierre,
                             p.concluido,
                             p.actividad,
                             p.area_responsable,
                             p.dato_cierre
                         }).ToList()
                        .Select(x => new TareaProyecto
                        {
                            tarea_id = x.tarea_id,
                            tarea_by_proy_id = x.tarea_by_proy_id,
                            proyecto_id = x.proyecto_id,
                            precedente = x.precedente,
                            fecha_compromiso = x.fecha_compromiso,
                            fecha_real_cierre = x.fecha_real_cierre,
                            concluido = x.concluido,
                            actividad = x.actividad.Trim(),
                            area_responsable = x.area_responsable.Trim(),
                            dato_cierre = x.dato_cierre.Trim()
                        }).Where(x => x.proyecto_id == id);
            if (!query.Any())
            {
                return NotFound();
            }
            return Ok(query);

        }

        // PUT: api/TareaProyectos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTareaProyecto(int id, TareaProyecto tareaProyecto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tareaProyecto.tarea_id)
            {
                return BadRequest();
            }

            db.Entry(tareaProyecto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TareaProyectoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TareaProyectos
        [ResponseType(typeof(TareaProyecto))]
        public IHttpActionResult PostTareaProyecto(TareaProyecto tareaProyecto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TareaProyecto.Add(tareaProyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TareaProyectoExists(tareaProyecto.tarea_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tareaProyecto.tarea_id }, tareaProyecto);
        }

        // DELETE: api/TareaProyectos/5
        [ResponseType(typeof(TareaProyecto))]
        public IHttpActionResult DeleteTareaProyecto(int id)
        {
            TareaProyecto tareaProyecto = db.TareaProyecto.Find(id);
            if (tareaProyecto == null)
            {
                return NotFound();
            }

            db.TareaProyecto.Remove(tareaProyecto);
            db.SaveChanges();

            return Ok(tareaProyecto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TareaProyectoExists(int id)
        {
            return db.TareaProyecto.Count(e => e.tarea_id == id) > 0;
        }
    }
}