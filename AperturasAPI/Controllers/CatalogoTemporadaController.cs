﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoTemporadaController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoTemporada
        [ResponseType(typeof(CAT_Temporada))]
        public IHttpActionResult GetCAT_Temporada()
        {
            var query = (from p in db.Set<CAT_Temporada>()
                         select new
                         {
                             p.temporada_id,
                             p.nombre_temporada
                         }).ToList()
                       .Select(x => new CAT_Temporada
                       {
                           nombre_temporada = x.nombre_temporada.Trim(),
                           temporada_id = x.temporada_id
                       });

            return Ok(query);
        }

        // GET: api/CatalogoTemporada/5
        [ResponseType(typeof(CAT_Temporada))]
        public IHttpActionResult GetCAT_Temporada(int id)
        {

            var query = (from p in db.Set<CAT_Temporada>()
                         select new
                         {
                             p.temporada_id,
                             p.nombre_temporada
                         }).ToList()
                       .Select(x => new CAT_Temporada
                       {
                           nombre_temporada = x.nombre_temporada.Trim(),
                           temporada_id = x.temporada_id
                       }).Where(x => x.temporada_id == id).FirstOrDefault();

            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoTemporada/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_Temporada(int id, CAT_Temporada cAT_Temporada)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_Temporada.temporada_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_Temporada).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_TemporadaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoTemporada
        [ResponseType(typeof(CAT_Temporada))]
        public IHttpActionResult PostCAT_Temporada(CAT_Temporada cAT_Temporada)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_Temporada.Add(cAT_Temporada);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_Temporada.temporada_id }, cAT_Temporada);
        }

        // DELETE: api/CatalogoTemporada/5
        [ResponseType(typeof(CAT_Temporada))]
        public IHttpActionResult DeleteCAT_Temporada(int id)
        {
            CAT_Temporada cAT_Temporada = db.CAT_Temporada.Find(id);
            if (cAT_Temporada == null)
            {
                return NotFound();
            }

            db.CAT_Temporada.Remove(cAT_Temporada);
            db.SaveChanges();

            return Ok(cAT_Temporada);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_TemporadaExists(int id)
        {
            return db.CAT_Temporada.Count(e => e.temporada_id == id) > 0;
        }
    }
}