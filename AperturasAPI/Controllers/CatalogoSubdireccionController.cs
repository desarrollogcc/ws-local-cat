﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoSubdireccionController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoSubdireccion
        [ResponseType(typeof(CAT_Subdireccion))]
        public IHttpActionResult GetCAT_Subdireccion()
        {
            var query = (from p in db.Set<CAT_Subdireccion>()
                         select new
                         {
                             p.subdireccion_id,
                             p.nombre_subdireccion
                         }).ToList()
                        .Select(x => new CAT_Subdireccion
                        {
                            nombre_subdireccion = x.nombre_subdireccion.Trim(),
                            subdireccion_id = x.subdireccion_id
                        });

            return Ok(query);
        }

        // GET: api/CatalogoSubdireccion/5
        [ResponseType(typeof(CAT_Subdireccion))]
        public IHttpActionResult GetCAT_Subdireccion(int id)
        {
            var query = (from p in db.Set<CAT_Subdireccion>()
                         select new
                         {
                             p.subdireccion_id,
                             p.nombre_subdireccion
                         }).ToList()
                       .Select(x => new CAT_Subdireccion
                       {
                           nombre_subdireccion = x.nombre_subdireccion.Trim(),
                           subdireccion_id = x.subdireccion_id
                       }).Where(x => x.subdireccion_id == id).FirstOrDefault();

            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoSubdireccion/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_Subdireccion(int id, CAT_Subdireccion cAT_Subdireccion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_Subdireccion.subdireccion_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_Subdireccion).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_SubdireccionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoSubdireccion
        [ResponseType(typeof(CAT_Subdireccion))]
        public IHttpActionResult PostCAT_Subdireccion(CAT_Subdireccion cAT_Subdireccion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_Subdireccion.Add(cAT_Subdireccion);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_Subdireccion.subdireccion_id }, cAT_Subdireccion);
        }

        // DELETE: api/CatalogoSubdireccion/5
        [ResponseType(typeof(CAT_Subdireccion))]
        public IHttpActionResult DeleteCAT_Subdireccion(int id)
        {
            CAT_Subdireccion cAT_Subdireccion = db.CAT_Subdireccion.Find(id);
            if (cAT_Subdireccion == null)
            {
                return NotFound();
            }

            db.CAT_Subdireccion.Remove(cAT_Subdireccion);
            db.SaveChanges();

            return Ok(cAT_Subdireccion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_SubdireccionExists(int id)
        {
            return db.CAT_Subdireccion.Count(e => e.subdireccion_id == id) > 0;
        }
    }
}