﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AperturasAPI.Models;

namespace AperturasAPI.Controllers
{
	

	public class CatalogoTipoTiendaController : ApiController
    {
        private AperturasEntities db = new AperturasEntities();

        // GET: api/CatalogoTipoTienda
        [ResponseType(typeof(CAT_TipoTienda))]
        public IHttpActionResult GetCAT_TipoTienda()
        {
            var query = (from p in db.Set<CAT_TipoTienda>()
                         select new
                         {
                             p.tipo_tienda_id,
                             p.nombre_tipo_tienda
                         }).ToList()
                       .Select(x => new CAT_TipoTienda
                       {
                           nombre_tipo_tienda = x.nombre_tipo_tienda.Trim(),
                           tipo_tienda_id = x.tipo_tienda_id
                       });


            return Ok(query);
        }

        // GET: api/CatalogoTipoTienda/5
        [ResponseType(typeof(CAT_TipoTienda))]
        public IHttpActionResult GetCAT_TipoTienda(int id)
        {
            var query = (from p in db.Set<CAT_TipoTienda>()
                         select new
                         {
                             p.tipo_tienda_id,
                             p.nombre_tipo_tienda
                         }).ToList()
                       .Select(x => new CAT_TipoTienda
                       {
                           nombre_tipo_tienda = x.nombre_tipo_tienda.Trim(),
                           tipo_tienda_id = x.tipo_tienda_id
                       }).Where(x => x.tipo_tienda_id == id);

            if (query == null)
            {
                return NotFound();
            }

            return Ok(query);
        }

        // PUT: api/CatalogoTipoTienda/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAT_TipoTienda(int id, CAT_TipoTienda cAT_TipoTienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAT_TipoTienda.tipo_tienda_id)
            {
                return BadRequest();
            }

            db.Entry(cAT_TipoTienda).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAT_TipoTiendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoTipoTienda
        [ResponseType(typeof(CAT_TipoTienda))]
        public IHttpActionResult PostCAT_TipoTienda(CAT_TipoTienda cAT_TipoTienda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAT_TipoTienda.Add(cAT_TipoTienda);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cAT_TipoTienda.tipo_tienda_id }, cAT_TipoTienda);
        }

        // DELETE: api/CatalogoTipoTienda/5
        [ResponseType(typeof(CAT_TipoTienda))]
        public IHttpActionResult DeleteCAT_TipoTienda(int id)
        {
            CAT_TipoTienda cAT_TipoTienda = db.CAT_TipoTienda.Find(id);
            if (cAT_TipoTienda == null)
            {
                return NotFound();
            }

            db.CAT_TipoTienda.Remove(cAT_TipoTienda);
            db.SaveChanges();

            return Ok(cAT_TipoTienda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAT_TipoTiendaExists(int id)
        {
            return db.CAT_TipoTienda.Count(e => e.tipo_tienda_id == id) > 0;
        }
    }
}