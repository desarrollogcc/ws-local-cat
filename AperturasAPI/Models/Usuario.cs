//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AperturasAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Usuario
    {
        public int no_empleado { get; set; }
        public string nombre { get; set; }
        public string email { get; set; }
        public string perfil { get; set; }
        public string cadena { get; set; }
        public string area_asignada { get; set; }
        public bool activo { get; set; }
        public bool reportes { get; set; }
        public string empleado_usuario { get; set; }
        public string password { get; set; }
    }
}
