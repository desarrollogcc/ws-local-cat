﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AperturasAPI.Models
{
    public class ProyectoInfo
    {
        public int ID;
        public String Tienda;
        public String TipoPlan;
        public String TipoTienda;
        public String TipoInmueble;
        public int TiendaEspejo;
        public DateTime FechaApertura;
        public String Direccion;
        public int Estatus;
        public bool EnComunicado;
    }
}