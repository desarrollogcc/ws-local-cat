﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AperturasAPI.Models
{
    public class ProyectoInfoFull
    {
        public int ID;
        public int TipoProyecto;
        public DateTime FechaApertura;
        public DateTime FechaInicioObra;
        public int Tienda;
        public String NombreTienda;
        public int Formato;
        public int Subdireccion;
        public int GerenteZona;
        public int TiendaEspejo;
        public int TipoTienda;
        public int TipoInmueble;
        public int ZonaTienda;
        public String Direccion;
        public String ObservacionesEspejo;
        public String ObservacionesGral;
    }
}